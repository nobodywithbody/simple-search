package search

import java.io.File
import java.util.*

val s = Scanner(System.`in`)

var file = File("")
var invertedIndex = mutableMapOf<String, MutableList<Int>>()

typealias SearchFunc = (String) -> Set<Int>

fun findPerson(searchFunction: SearchFunc) {
    println("Enter a name or email to search all suitable people.")
    s.nextLine()
    val search = s.nextLine()
    val lines = searchFunction(search)

    if (lines.isEmpty()) {
        println("Noting found")
        return
    }
    println("${lines.size} persons found:\n")
    var lineNumber = 0
    file.forEachLine {
        if (lineNumber in lines) {
            println(it)
        }

        lineNumber++
    }
}

fun printAll(file: File) {
    println("\n=== List of people ===")
    file.forEachLine { println(it) }
}

fun findArg(name: String, args: Array<String>): String {
    val index = args.indexOfFirst { it == "--$name" }

    if (index == -1) throw Exception("Empty data arg")

    if (args.size <= index + 1) throw Exception("Args list is small")

    return args[index + 1]
}

fun createInvertedIndex(file: File): MutableMap<String, MutableList<Int>> {
    var count = 0
    val invertedIndex = mutableMapOf<String, MutableList<Int>>()
    file.forEachLine { s1 ->
        val words = s1.split(' ')
        words.forEach {
            if (invertedIndex[it.toLowerCase()] == null) invertedIndex[it.toLowerCase()] = mutableListOf(count)
            else invertedIndex[it.toLowerCase()]?.add(count)
        }

        count++
    }

    return invertedIndex
}

fun findAll(search: String): Set<Int> {
    var uniqueLines = setOf<Int>()

    for (i in search.split(' ')) {
        val wordLines = invertedIndex[i.toLowerCase()]

        if (wordLines == null) {
            break
        }

        if (uniqueLines.isEmpty()) {
            uniqueLines = wordLines.toSet()
        }
        uniqueLines =  uniqueLines intersect wordLines
    }

    return uniqueLines
}

fun findAny(search: String): Set<Int> {
    val lines = mutableSetOf<Int>()

    for (i in search.split(' ')) {
        val wordLines = invertedIndex[i.toLowerCase()]

        if (wordLines == null) {
            continue
        }

        lines +=  wordLines.toSet()
    }

    return lines
}

fun findNone(search: String): Set<Int> {
    val excludeLines = mutableSetOf<Int>()
    val includeLines = mutableSetOf<Int>()
    val words = search.split(' ').map { it.toLowerCase() }

    for ((word, lines) in invertedIndex) {
        if (word !in words) includeLines += lines
        else excludeLines += lines
    }

    return includeLines - excludeLines
}

fun getModeFunc(mode: String): SearchFunc = when (mode) {
    "ALL" -> ::findAll
    "ANY" -> ::findAny
    "NONE" -> ::findNone
    else -> throw Exception("Wrong mode")
}

fun main(args: Array<String>) {
    val path = findArg("data", args)
    file = File(path)

    if (!file.isFile) {
        throw Exception("File not exists")
    }

    invertedIndex = createInvertedIndex(file)

    loop@ while (true) {
        println("\n=== Menu ===")
        println("1. Find a person")
        println("2. Print all people")
        println("0. Exit")

        when (s.next()) {
            "1" -> {
                println("Select a matching strategy: ALL, ANY, NONE")
                val mode = s.next()
                findPerson(getModeFunc(mode))
            }
            "2" -> printAll(file)
            "0" -> break@loop
            else -> println("\nIncorrect option! Try again.")
        }
    }

    println("Bye!")
}
